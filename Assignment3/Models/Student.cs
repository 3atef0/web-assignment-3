﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Assignment3.Models
{
    public class Student
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int Grades { get; set; }

    }
}