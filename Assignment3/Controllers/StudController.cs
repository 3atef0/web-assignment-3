﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment3.Models;

namespace Assignment3.Controllers
{
    public class StudController : Controller
    {
        // GET: Stud
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult StdInfo()
        {
            //Querying with LINQ to Entities 
            using (var context = new StudentContext())
            {
                var query = context.Students.FirstOrDefault<Student>();
                ViewBag.student = query;

            }
            return View();
        }
    }
}